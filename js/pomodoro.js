function secondsToTime(seconds) {
    var time = {minutes: 0, seconds: 0};

    if (seconds > 60) {
        time.minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;
    }

    time.seconds = seconds

    return time;
}

function sec2min(sec) {
    return Math.floor(sec/60);
}

function format_display(num) {
    if (num < 10) {
        num = "0" + num;
    }

    return num;
}

function timerDisplay(el) {
    var timer_display = el;

    var minutes = document.createElement("span");
    minutes.innerHTML = "00";

    var delimiter = document.createElement("span");
    delimiter.innerHTML = ":";

    var seconds = document.createElement("span");
    seconds.innerHTML = "00";

    timer_display.appendChild(minutes);
    timer_display.appendChild(delimiter);
    timer_display.appendChild(seconds);

    this.set_time = function (sec) {
        var time = secondsToTime(sec);

        minutes.innerHTML = format_display(time.minutes);
        seconds.innerHTML = format_display(time.seconds);
    };
}


function toggleButton(el, options) {
    var self = this;
    options = options || {};
    self.button_active_class = options.active_class || "active";
    self.active = options.active || false;

    var inactive_label = options.inactive_label || "inactive";
    var active_label = options.active_label || "active";

    this.button = el;
    this.button.innerHTML = inactive_label;

    this.activate_hook = options.activate_hook || function () {
        return undefined;
    };

    this.disactivate_hook = options.disactivate_hook || function () {
        return undefined;
    };

    this.activate = function () {
        self.active = true;
        self.button.classList.add(self.button_active_class);
        self.button.innerHTML = active_label;
        self.activate_hook();
    };

    this.disactivate = function () {
        self.active = false;
        self.button.classList.remove(self.button_active_class);
        self.button.innerHTML = inactive_label;
        self.disactivate_hook();
    };

    if (self.active) {
        self.activate();
    } else {
        self.disactivate();
    }

    this.button.onclick = function () {
        if (!self.active) {
            self.activate();
        } else {
            self.disactivate();
        }
    };
}


function Timer(options) {
    var self = this;
    options = options || {};
    self.timerDoneEvent = new Event("timer_done");
    self.tickEvent = new Event("tick");

    self.timerId = null;

    self.seconds = options.seconds || 0;
    self.now_seconds = 0;
    self.countdown = options.countdown || false;
    self.tick_hook = options.tick_hook || function () { console.log(self.now_seconds); };
    self.end_hook = options.end_hook || function () { console.log("Время вышло"); };
    self.isRunning = false;

    this.start = function() {
        self.countdown = true;
        self.now_seconds = self.seconds;
        timer_run();
        self.isRunning = true;
    };

    this.getMinutes = function() {
        return sec2min(self.now_seconds);
    }

    function timer_run() {
        if (!self.countdown) {
            return;
        }

        self.tick_hook(self.now_seconds, self.seconds);

        if (self.now_seconds < 1) {
            self.end_hook(self.seconds);
            var audio = new Audio("ring_bell_short.mp3");
            audio.play();
            self.countdown = false;
            document.dispatchEvent(self.timerDoneEvent);
        } else {
            self.timerId = setTimeout(timer_run, 1000);
            --self.now_seconds;
        }
    }

    this.stop = function () {
        clearTimeout(self.timerId);
        self.countdown = false;
        self.isRunning = false;
        self.now_seconds = self.seconds;
    };
}


var hintLabel = {
    label: "",
    getElement: function() {
        return document.querySelector(".timer__hint");
    },
    setText: function(text) {
        this.getElement().innerHTML = text;
    },
    clearText: function() {
        this.setText("");
    }
};


function timeChooser(el, options) {
    var self = this;
    options = options || {};

    self.chooseTimeEvent = new Event("choose_time_event");
    self.values = options.values || [];
    self.labels = options.labels || [];
    self.container = el;
    self.checked = 0 || options.checked;
    self.name = options.name || "time_choose";

    self.select_value_hook = options.select_value_hook || function () { console.log(self.getValue()); };

    function select_value() {
        document.dispatchEvent(self.chooseTimeEvent);
        self.select_value_hook();
    }

    for(var i=0; i<self.values.length; i++) {
        var radio = document.createElement("input");
        radio.setAttribute("type", 'radio');
        radio.setAttribute("name", self.name);
        radio.setAttribute("id", self.name + "_" + i);
        radio.setAttribute('value', self.values[i]);
        self.container.appendChild(radio);
        radio.onclick = select_value;

        if (i === self.checked) {
            radio.checked = true;
        }

        var label = document.createElement("label");
        label.setAttribute("for", self.name + "_" + i);
        label.setAttribute("class", self.name + "__variant");
        label.innerHTML = self.labels[i] || self.values[i];
        self.container.appendChild(label);
    }

    self.getValue = function() {
        var radio = self.container.querySelector("input:checked");

        if (!radio) {
            radio = self.container.querySelector("input");
        }
        return radio.getAttribute("value");
    };

    self.setValue = function(value) {
        var input = self.container.querySelector("input[value='" + value + "']");
        select_value();
        input.checked = true;
    }
}


function setTimerInTitle(seconds) {
    var title = document.querySelector("title");
    var time = secondsToTime(seconds);

    title.innerHTML = "[" +
        format_display(time.minutes) +
        ":" +
        format_display(time.seconds) +
        "] " +
        title.innerHTML.replace(/\[.*\]/, "");
}


function getIndicator(val, max) {
    var indicators = [
        "icon-25-00.png",
        "icon-25-01.png",
        "icon-25-02.png",
        "icon-25-03.png",
        "icon-25-04.png",
        "icon-25-05.png",
        "icon-25-06.png",
        "icon-25-07.png",
        "icon-25-08.png",
        "icon-25-09.png",
        "icon-25-10.png",
        "icon-25-11.png",
        "icon-25-12.png",
        "icon-25-13.png",
        "icon-25-14.png",
        "icon-25-15.png",
        "icon-25-16.png",
        "icon-25-17.png",
        "icon-25-18.png",
        "icon-25-19.png",
        "icon-25-20.png",
        "icon-25-21.png",
        "icon-25-22.png",
        "icon-25-23.png",
        "icon-25-24.png",
    ];

    var section = 25 / max;
    return indicators[Math.round(val * section)];
}


function setFavicon(favicon_path) {
    document.querySelector("head link[rel=icon]").href = favicon_path;
}


// const SECONDS_IN_MINUTE = 1; // for tests
const SECONDS_IN_MINUTE = 60;

const POMODORO_TIME = 25*SECONDS_IN_MINUTE;
const SHORT_PAUSE = 5*SECONDS_IN_MINUTE;
const LONG_PAUSE = 15*SECONDS_IN_MINUTE;

(function () {
    var hintText = hintLabel;
    var pomodoroCounter = 0;

    var timer_display = new timerDisplay(
        document.querySelector("#pomodoroTimer .timer__display")
    );

    var time_chooser = new timeChooser(
        document.querySelector(".timer__time_choose"),
        {
            labels: ["25 минут", "15 минут", "5 минут"],
            values: [POMODORO_TIME, LONG_PAUSE, SHORT_PAUSE],
            checked: 0,
            select_value_hook: function() {
                timer.stop();
                startButton.disactivate();
                setFavicon("gfx/" + getIndicator(
                    25,
                    25
                ));
            }
        }
    );

    timer_display.set_time(time_chooser.getValue());

    var timer = new Timer({
        seconds: time_chooser.getValue(),
        tick_hook: function (now_seconds, seconds) {
            var self = this;
            timer_display.set_time(this.now_seconds);
            setFavicon("gfx/" + getIndicator(
                timer.getMinutes(),
                sec2min(time_chooser.getValue())
            ));
            setTimerInTitle(this.now_seconds);

            if (seconds == POMODORO_TIME) {
                hintText.setText(MESSAGES.POMODORO_START_MESSAGE);
            }
        },
        end_hook: function (seconds) {
            setFavicon("gfx/" + getIndicator(
                0,
                sec2min(time_chooser.getValue())
            ));

            if (seconds == POMODORO_TIME) {
                pomodoroCounter += 1;
                if ( !(pomodoroCounter % 4) ) {
                    hintText.setText(MESSAGES.LONG_PAUSE_MESSAGE);
                    time_chooser.setValue(LONG_PAUSE);
                    timer.seconds = time_chooser.getValue();
                    timer_display.set_time(time_chooser.getValue());
                } else {
                    hintText.setText(MESSAGES.SHORT_PAUSE_MESSAGE);
                    time_chooser.setValue(SHORT_PAUSE);
                    timer.seconds = time_chooser.getValue();
                    timer_display.set_time(time_chooser.getValue());
                }
            } else if (seconds == SHORT_PAUSE) {
                hintText.setText("");
                time_chooser.setValue(POMODORO_TIME);
                timer.seconds = time_chooser.getValue();
                timer_display.set_time(time_chooser.getValue());
            }
        }
    });

    var startButton = new toggleButton(
        document.querySelector("#pomodoroTimer .timer__start_btn"),
        {
            inactive_label: "Старт",
            active_label: "Стоп",
            activate_hook: function () {
                timer.start();
            },
            disactivate_hook: function () {
                timer.stop();
            }
        }
    );

    document.addEventListener(time_chooser.chooseTimeEvent.type, function() {
        setTimerInTitle(time_chooser.getValue());
        timer_display.set_time(time_chooser.getValue());
        timer.seconds = time_chooser.getValue();
    }, false);

    document.addEventListener(timer.timerDoneEvent.type, function () {
        startButton.disactivate();
    }, false);

    document.querySelector("body").onkeydown = function(e) {
        if (e.code == "Space") {
            if (timer.isRunning) {
                startButton.disactivate();
                timer.stop();
            } else {
                startButton.activate();
                timer.start();
            }
        }
    }
})();
