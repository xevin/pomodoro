MESSAGES = {
    POMODORO_START_MESSAGE: "Сосредоточься на задаче",
    LONG_PAUSE_MESSAGE: "Теперь можно сделать большой перерыв",
    SHORT_PAUSE_MESSAGE: "Отдохни 5 минут"
}
